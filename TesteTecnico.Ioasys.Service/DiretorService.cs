﻿using System.Threading.Tasks;
using TesteTecnico.Ioasys.Domain.Contract.Repository;
using TesteTecnico.Ioasys.Domain.Contract.Service;
using TesteTecnico.Ioasys.Domain.Model;
using TesteTecnico.Ioasys.Domain.Model.Entity;

namespace TesteTecnico.Ioasys.Service
{
    public class DiretorService : IDiretorService
    {
        private IDiretorRepository DiretorRepository;

        public DiretorService(IDiretorRepository diretorRepository)
        {
            DiretorRepository = diretorRepository;
        }
        public async Task<ListResponse<Diretor>> FilterDiretoresAsync(string nome, int page = 0, int pageCount = 10)
        {
            return await DiretorRepository.FilterDiretoresAsync(nome, page, pageCount);
        }

        public async Task<Diretor> GetById(string idDiretor)
        {
            return await DiretorRepository.GetDiretorById(idDiretor);
        }

        public async Task<Diretor> InsertDiretorAsync(Diretor diretor)
        {
            return await DiretorRepository.InsertAsync(diretor);
        }

        public async Task<Diretor> UpdateDiretorAsync(Diretor diretor)
        {
            return await DiretorRepository.UpdateAsync(diretor);
        }
    }
}
