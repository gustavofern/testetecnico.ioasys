﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TesteTecnico.Ioasys.Domain.Contract.Repository;
using TesteTecnico.Ioasys.Domain.Contract.Service;
using TesteTecnico.Ioasys.Domain.Model;
using TesteTecnico.Ioasys.Domain.Model.Entity;
using TesteTecnico.Ioasys.Domain.Model.RO;

namespace TesteTecnico.Ioasys.Service
{
    public class FilmeService : IFilmeService
    {
        private IFilmeRepository FilmeRepository;

        public FilmeService(IFilmeRepository filmeRepository)
        {
            FilmeRepository = filmeRepository;
        }

        public async Task<Filme> GetById(string idFilme)
        {
            return await FilmeRepository.GetFilmeById(idFilme);
        }

        public async Task<ListResponse<Filme>> FilterFilmesAsync(string nome, string diretor, List<string> generos, List<string> atores, int page = 0, int pageCount = 10)
        {
            return await FilmeRepository.FilterFilmesAsync(nome, diretor, generos, atores, page, pageCount);
        }

        public async Task<Filme> VoteAsync(string idFilme, int nota)
        {
            if (nota < 0 || nota > 4)
            {
                throw new Exception("Nota inválida");
            }

            var filme = await FilmeRepository.GetFilmeById(idFilme);

            filme.QuantidadeVotos++;
            filme.QualificacaoTotal += nota;

            return await FilmeRepository.UpdateAsync(filme);
        }

        public async Task<Filme> InsertFilmeAsync(NewFilmeRequestObject requestObj)
        {
            var newId = Guid.NewGuid().ToString();
            return await FilmeRepository.InsertAsync(new Filme
            {
                IdFilme = newId,
                Nome = requestObj.Nome,
                DataLancamento = requestObj.DataLancamento,
                Resumo = requestObj.Resumo,
                Atores = requestObj.Atores.Select(a => new AtorFilme { IdAtor = a, IdFilme = newId }).ToList(),
                Generos = requestObj.Generos.Select(a => new GeneroFilme { IdGenero = a, IdFilme = newId }).ToList()
            });
        }

        public async Task<Filme> UpdateFilmeAsync(Filme filme)
        {
            return await FilmeRepository.UpdateAsync(filme);
        }
    }
}
