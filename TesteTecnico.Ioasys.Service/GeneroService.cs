﻿using System.Threading.Tasks;
using TesteTecnico.Ioasys.Domain.Contract.Repository;
using TesteTecnico.Ioasys.Domain.Contract.Service;
using TesteTecnico.Ioasys.Domain.Model;
using TesteTecnico.Ioasys.Domain.Model.Entity;

namespace TesteTecnico.Ioasys.Service
{
    public class GeneroService: IGeneroService
    {
        private IGeneroRepository GeneroRepository;

        public GeneroService(IGeneroRepository generoRepository)
        {
            GeneroRepository = generoRepository;
        }
        public async Task<ListResponse<Genero>> FilterGenerosAsync(string nome, int page = 0, int pageCount = 10)
        {
            return await GeneroRepository.FilterGenerosAsync(nome, page, pageCount);
        }

        public async Task<Genero> GetById(string idGenero)
        {
            return await GeneroRepository.GetGeneroById(idGenero);
        }

        public async Task<Genero> InsertGeneroAsync(Genero genero)
        {
            return await GeneroRepository.InsertAsync(genero);
        }

        public async Task<Genero> UpdateGeneroAsync(Genero genero)
        {
            return await GeneroRepository.UpdateAsync(genero);
        }
    }
}
