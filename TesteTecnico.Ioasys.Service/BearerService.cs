﻿using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using TesteTecnico.Ioasys.Domain.Contract.Service;
using TesteTecnico.Ioasys.Domain.Model.Entity;

namespace TesteTecnico.Ioasys.Service
{
    public class BearerService : IBearerService
    {
        private IConfiguration Configuration;
        public BearerService(IConfiguration configuration) => Configuration = configuration;

        public dynamic GenerateBearer(Usuario usuario)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(Configuration["TokenConfigurations:IssuerSigningKey"]);

            var identity = new ClaimsIdentity(
                    new[] {
                        new Claim(ClaimTypes.Email, usuario.Email)
                    });

            identity.AddClaim(new Claim("idUsuario", usuario.IdUsuario));
            identity.AddClaim(new Claim("Administrador", usuario.Administrador ? "True" : "False"));
            identity.AddClaim(new Claim("nome", usuario.Nome));

            var dataCriacao = DateTime.UtcNow.AddHours(-3);
            var dataExpiracao = dataCriacao.AddDays(Convert.ToDouble(Configuration["TokenConfigurations:Days"]));

            var securityToken = tokenHandler.CreateToken(new SecurityTokenDescriptor
            {
                Issuer = Configuration["TokenConfigurations:Issuer"],
                Audience = Configuration["TokenConfigurations:Audience"],
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature),
                Subject = identity,
                IssuedAt = dataCriacao,
                NotBefore = dataCriacao,
                Expires = dataExpiracao,
            });
            var token = tokenHandler.WriteToken(securityToken);

            return new
            {
                Created = dataCriacao.ToString("yyyy-MM-dd HH:mm:ss"),
                Expiration = dataExpiracao.ToString("yyyy-MM-dd HH:mm:ss"),
                Access_token = token,
                Message = "Bem vindo!"
            };
        }
    }
}
