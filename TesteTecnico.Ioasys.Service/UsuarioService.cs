﻿using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using TesteTecnico.Ioasys.Domain.Contract.Repository;
using TesteTecnico.Ioasys.Domain.Contract.Service;
using TesteTecnico.Ioasys.Domain.Model;
using TesteTecnico.Ioasys.Domain.Model.Entity;

namespace TesteTecnico.Ioasys.Service
{
    public class UsuarioService : IUsuarioService
    {
        private IUsuarioRepository UsuarioRepository;

        public UsuarioService(IUsuarioRepository usuarioRepository)
        {
            UsuarioRepository = usuarioRepository;
        }

        public async Task<Usuario> GetByEmailSenha(string email, string senha)
        {
            return await UsuarioRepository.GetUsuarioByEmailSenha(email, EncryptPassword(senha));
        }

        public async Task<Usuario> GetById(string idUsuario)
        {
            return await UsuarioRepository.GetUsuarioById(idUsuario);
        }

        public async Task<ListResponse<Usuario>> ListNonAdminUsersAsync(int page = 0, int pageCount = 10)
        {
            return await UsuarioRepository.ListNonAdminUsersAsync(page, pageCount);
        }

        public async Task<Usuario> InsertUsuarioAsync(Usuario usuario)
        {
            usuario.Senha = EncryptPassword(usuario.Senha);

            return await UsuarioRepository.InsertAsync(usuario);
        }

        public async Task<Usuario> UpdateUsuarioAsync(Usuario usuario)
        {
            var user = await UsuarioRepository.GetUsuarioById(usuario.IdUsuario);

            if (string.IsNullOrEmpty(usuario.Senha))
            {
                usuario.Senha = user.Senha;
            }
            else if (usuario.Senha != user.Senha)
            {
                usuario.Senha = EncryptPassword(usuario.Senha);
            }

            return await UsuarioRepository.UpdateAsync(usuario);
        }

        public async Task<Usuario> DisableUsuarioAsync(string idUsuario)
        {
            var usuario = await UsuarioRepository.GetUsuarioById(idUsuario);

            usuario.Ativo = false;

            return await UsuarioRepository.UpdateAsync(usuario);
        }

        private string EncryptPassword(string password)
        {
            var crypt = new SHA256Managed();
            var hash = new StringBuilder();
            byte[] crypto = crypt.ComputeHash(Encoding.UTF8.GetBytes(password));
            foreach (byte theByte in crypto)
            {
                hash.Append(theByte.ToString("x2"));
            }
            return hash.ToString();
        }
    }
}
