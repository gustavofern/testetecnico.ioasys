﻿using System.Threading.Tasks;
using TesteTecnico.Ioasys.Domain.Contract.Repository;
using TesteTecnico.Ioasys.Domain.Contract.Service;
using TesteTecnico.Ioasys.Domain.Model;
using TesteTecnico.Ioasys.Domain.Model.Entity;

namespace TesteTecnico.Ioasys.Service
{
    public class AtorService: IAtorService
    {
        private IAtorRepository AtorRepository;

        public AtorService(IAtorRepository atorRepository)
        {
            AtorRepository = atorRepository;
        }
        public async Task<ListResponse<Ator>> FilterAtoresAsync(string nome, int page = 0, int pageCount = 10)
        {
            return await AtorRepository.FilterAtoresAsync(nome, page, pageCount);
        }

        public async Task<Ator> GetById(string idAtor)
        {
            return await AtorRepository.GetAtorById(idAtor);
        }

        public async Task<Ator> InsertAtorAsync(Ator ator)
        {
            return await AtorRepository.InsertAsync(ator);
        }

        public async Task<Ator> UpdateAtorAsync(Ator ator)
        {
            return await AtorRepository.UpdateAsync(ator);
        }
    }
}
