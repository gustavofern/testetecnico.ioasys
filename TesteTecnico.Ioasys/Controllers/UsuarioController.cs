﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using TesteTecnico.Ioasys.Domain.Contract;
using TesteTecnico.Ioasys.Domain.Contract.Service;
using TesteTecnico.Ioasys.Domain.Model.Entity;

namespace TesteTecnico.Ioasys.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class UsuarioController : ControllerBase
    {
        private readonly IBearerService BearerService;
        private readonly IUsuarioService UsuarioService;
        private readonly ILoggedUser LoggedUser;

        public UsuarioController(IBearerService bearerService, IUsuarioService usuarioService, ILoggedUser loggedUser)
        {
            BearerService = bearerService;
            UsuarioService = usuarioService;
            LoggedUser = loggedUser;
        }

        /// <summary>
        /// Lista os usuários não administradores páginados
        /// </summary>
        /// <param name="page">inicia no 0</param>
        /// <param name="pageCount">padrão é 10 por página</param>
        /// <returns></returns>
        [Authorize]
        [HttpGet("list")]
        public async Task<IActionResult> ListNonAdminUsersAsync([FromQuery] int page = 0, [FromQuery] int pageCount = 10)
        {
            try
            {
                if (!LoggedUser.Administrador)
                    return Unauthorized("Usuário não possui permissão para esta ação.");

                return Ok(await UsuarioService.ListNonAdminUsersAsync(page, pageCount));
            }
            catch
            {
                return BadRequest("Erro ao listar usuários");
            }
        }

        /// <summary>
        /// Retorna o usuário atual
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpGet("current")]
        public async Task<IActionResult> GetCurrentUserAsync()
        {
            try
            {
                return Ok(await UsuarioService.GetById(LoggedUser.IdUsuario));
            }
            catch (Exception)
            {
                return BadRequest("Erro ao buscar usuário");
            }
        }

        /// <summary>
        /// Cria um novo usuário
        /// </summary>
        /// <param name="usuario"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateNewUserAsync([FromBody] Usuario usuario)
        {
            try
            {
                return Ok(await UsuarioService.InsertUsuarioAsync(usuario));
            }
            catch (Exception)
            {
                return BadRequest("Erro ao salvar usuário");
            }
        }

        /// <summary>
        /// Atualiza o usuário atual
        /// </summary>
        /// <param name="usuario"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPut]
        public async Task<IActionResult> UpdateUserAsync([FromBody] Usuario usuario)
        {
            try
            {
                if (usuario.IdUsuario != LoggedUser.IdUsuario)
                    return Unauthorized("Este usuário não tem permissão de editar outro usuário.");

                return Ok(await UsuarioService.UpdateUsuarioAsync(usuario));
            }
            catch (Exception)
            {
                return BadRequest("Erro ao salvar usuário");
            }
        }

        /// <summary>
        /// Desabilita o usuário atual
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpPut("disable")]
        public async Task<IActionResult> DisableUserAsync()
        {
            try
            {
                return Ok(await UsuarioService.DisableUsuarioAsync(LoggedUser.IdUsuario));

                // TO DO: invalidate current Bearer
            }
            catch (Exception)
            {
                return BadRequest("Erro ao desabilitar usuário");
            }
        }

        /// <summary>
        /// Fazer login
        /// </summary>
        /// <param name="email"></param>
        /// <param name="senha"></param>
        /// <returns></returns>
        [HttpPut("login")]
        public async Task<IActionResult> LoginAsync([FromQuery] string email, [FromQuery] string senha)
        {
            try
            {
                var usuario = await UsuarioService.GetByEmailSenha(email, senha);

                if (usuario != null)
                {
                    return Ok(BearerService.GenerateBearer(usuario));
                }

                return BadRequest("Email ou senha incorretos");
            }
            catch (Exception)
            {
                return BadRequest("Erro ao fazer login");
            }
        }
    }
}
