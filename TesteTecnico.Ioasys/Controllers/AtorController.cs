﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using TesteTecnico.Ioasys.Domain.Contract;
using TesteTecnico.Ioasys.Domain.Contract.Service;
using TesteTecnico.Ioasys.Domain.Model.Entity;

namespace TesteTecnico.Ioasys.WebApi.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class AtorController : ControllerBase
    {
        private readonly IAtorService AtorService;
        private readonly ILoggedUser LoggedUser;

        public AtorController(IAtorService atorService, ILoggedUser loggedUser)
        {
            AtorService = atorService;
            LoggedUser = loggedUser;
        }

        /// <summary>
        /// Filtrar atores
        /// </summary>
        /// <param name="nome">Nome do ator</param>
        /// <param name="page">Inicia no 0</param>
        /// <param name="pageCount">Quantidade padrão é 10</param>
        /// <returns></returns>
        [HttpGet("filter")]
        public async Task<IActionResult> FilterAtoresAsync([FromQuery] string nome, [FromQuery] int page = 0, [FromQuery] int pageCount = 10)
        {
            try
            {
                return Ok(await AtorService.FilterAtoresAsync(nome, page, pageCount));
            }
            catch
            {
                return BadRequest("Erro ao listar os atores");
            }
        }

        /// <summary>
        /// Retorna os detalhes do ator
        /// </summary>
        /// <param name="idAtor"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetAtorById([FromQuery] string idAtor)
        {
            try
            {
                return Ok(await AtorService.GetById(idAtor));
            }
            catch
            {
                return BadRequest("Erro ao buscar o ator");
            }
        }

        /// <summary>
        /// Criar novo ator
        /// </summary>
        /// <param name="ator"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        public async Task<IActionResult> CreateNewAtorAsync([FromBody] Ator ator)
        {
            try
            {
                if (!LoggedUser.Administrador)
                    return Unauthorized("Usuário não possui permissão para essa ação");

                return Ok(await AtorService.InsertAtorAsync(ator));
            }
            catch (Exception)
            {
                return BadRequest("Erro ao salvar ator");
            }
        }

        /// <summary>
        /// Atualizar ator
        /// </summary>
        /// <param name="ator"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPut]
        public async Task<IActionResult> UpdateAtorAsync([FromBody] Ator ator)
        {
            try
            {
                if (!LoggedUser.Administrador)
                    return Unauthorized("Usuário não possui permissão para essa ação");

                return Ok(await AtorService.UpdateAtorAsync(ator));
            }
            catch (Exception)
            {
                return BadRequest("Erro ao editar ator");
            }
        }
    }
}
