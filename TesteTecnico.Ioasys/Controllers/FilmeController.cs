﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TesteTecnico.Ioasys.Domain.Contract;
using TesteTecnico.Ioasys.Domain.Contract.Service;
using TesteTecnico.Ioasys.Domain.Model.Entity;
using TesteTecnico.Ioasys.Domain.Model.RO;

namespace TesteTecnico.Ioasys.WebApi.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class FilmeController : ControllerBase
    {
        private readonly IFilmeService FilmeService;
        private readonly ILoggedUser LoggedUser;

        public FilmeController(IFilmeService filmeService, ILoggedUser loggedUser)
        {
            FilmeService = filmeService;
            LoggedUser = loggedUser;
        }

        /// <summary>
        /// Filtrar filmes
        /// </summary>
        /// <param name="nome">Nome do filme</param>
        /// <param name="diretor">Id do diretor</param>
        /// <param name="generos">Lista com id dos generos</param>
        /// <param name="atores">Lista com id dos atores</param>
        /// <param name="page">Inicia no 0</param>
        /// <param name="pageCount">Quantidade padrão é 10</param>
        /// <returns></returns>
        [HttpGet("filter")]
        public async Task<IActionResult> FilterFilmesAsync([FromQuery] string nome, [FromQuery] string diretor, [FromQuery] List<string> generos, [FromQuery] List<string> atores, [FromQuery] int page = 0, [FromQuery] int pageCount = 10)
        {
            try
            {
                return Ok(await FilmeService.FilterFilmesAsync(nome, diretor, generos, atores, page, pageCount));
            }
            catch
            {
                return BadRequest("Erro ao listar os filmes");
            }
        }

        /// <summary>
        /// Retorna os detalhes completos do filme
        /// </summary>
        /// <param name="idFilme"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetFilmeById([FromQuery] string idFilme)
        {
            try
            {
                return Ok(await FilmeService.GetById(idFilme));
            }
            catch
            {
                return BadRequest("Erro ao buscar o filme");
            }
        }

        /// <summary>
        /// Votar no filme
        /// </summary>
        /// <param name="idFilme">id do filme</param>
        /// <param name="nota">nota de 0-4</param>
        /// <returns></returns>
        [Authorize]
        [HttpPut("vote")]
        public async Task<IActionResult> VoteOnFilmeAsync([FromQuery] string idFilme, [FromQuery] int nota)
        {
            try
            {
                if (LoggedUser.Administrador)
                    return Unauthorized("Usuários administradores não podem votar nos filmes.");

                return Ok(await FilmeService.VoteAsync(idFilme, nota));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Criar novo filme
        /// </summary>
        /// <param name="filme"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        public async Task<IActionResult> CreateNewFilmeAsync([FromBody] NewFilmeRequestObject filme)
        {
            try
            {
                if (!LoggedUser.Administrador)
                    return Unauthorized("Usuário não possui permissão para essa ação");

                return Ok(await FilmeService.InsertFilmeAsync(filme));
            }
            catch (Exception)
            {
                return BadRequest("Erro ao salvar filme");
            }
        }

        /// <summary>
        /// Atualizar filme
        /// </summary>
        /// <param name="filme"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPut]
        public async Task<IActionResult> UpdateFilmeAsync([FromBody] Filme filme)
        {
            try
            {
                if (!LoggedUser.Administrador)
                    return Unauthorized("Usuário não possui permissão para essa ação");

                return Ok(await FilmeService.UpdateFilmeAsync(filme));
            }
            catch (Exception)
            {
                return BadRequest("Erro ao editar filme");
            }
        }
    }
}
