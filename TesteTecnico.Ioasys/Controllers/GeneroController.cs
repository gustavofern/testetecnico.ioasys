﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using TesteTecnico.Ioasys.Domain.Contract;
using TesteTecnico.Ioasys.Domain.Contract.Service;
using TesteTecnico.Ioasys.Domain.Model.Entity;

namespace TesteTecnico.Ioasys.WebApi.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class GeneroController : ControllerBase
    {
        private readonly IGeneroService GeneroService;
        private readonly ILoggedUser LoggedUser;

        public GeneroController(IGeneroService generoService, ILoggedUser loggedUser)
        {
            GeneroService = generoService;
            LoggedUser = loggedUser;
        }

        /// <summary>
        /// Filtrar generos
        /// </summary>
        /// <param name="nome">Nome do genero</param>
        /// <param name="page">Inicia no 0</param>
        /// <param name="pageCount">Quantidade padrão é 10</param>
        /// <returns></returns>
        [HttpGet("filter")]
        public async Task<IActionResult> FilterGenerosAsync([FromQuery] string nome, [FromQuery] int page = 0, [FromQuery] int pageCount = 10)
        {
            try
            {
                return Ok(await GeneroService.FilterGenerosAsync(nome, page, pageCount));
            }
            catch
            {
                return BadRequest("Erro ao listar os generoes");
            }
        }

        /// <summary>
        /// Retorna os detalhes do genero
        /// </summary>
        /// <param name="idGenero"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetGeneroById([FromQuery] string idGenero)
        {
            try
            {
                return Ok(await GeneroService.GetById(idGenero));
            }
            catch
            {
                return BadRequest("Erro ao buscar o genero");
            }
        }

        /// <summary>
        /// Criar novo genero
        /// </summary>
        /// <param name="genero"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        public async Task<IActionResult> CreateNewGeneroAsync([FromBody] Genero genero)
        {
            try
            {
                if (!LoggedUser.Administrador)
                    return Unauthorized("Usuário não possui permissão para essa ação");

                return Ok(await GeneroService.InsertGeneroAsync(genero));
            }
            catch (Exception)
            {
                return BadRequest("Erro ao salvar genero");
            }
        }

        /// <summary>
        /// Atualizar genero
        /// </summary>
        /// <param name="genero"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPut]
        public async Task<IActionResult> UpdateGeneroAsync([FromBody] Genero genero)
        {
            try
            {
                if (!LoggedUser.Administrador)
                    return Unauthorized("Usuário não possui permissão para essa ação");

                return Ok(await GeneroService.UpdateGeneroAsync(genero));
            }
            catch (Exception)
            {
                return BadRequest("Erro ao editar genero");
            }
        }
    }
}