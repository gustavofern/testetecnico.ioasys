﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using TesteTecnico.Ioasys.Domain.Contract;
using TesteTecnico.Ioasys.Domain.Contract.Service;
using TesteTecnico.Ioasys.Domain.Model.Entity;

namespace TesteTecnico.Ioasys.WebApi.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class DiretorController : ControllerBase
    {
        private readonly IDiretorService DiretorService;
        private readonly ILoggedUser LoggedUser;

        public DiretorController(IDiretorService diretorService, ILoggedUser loggedUser)
        {
            DiretorService = diretorService;
            LoggedUser = loggedUser;
        }

        /// <summary>
        /// Filtrar diretores
        /// </summary>
        /// <param name="nome">Nome do diretor</param>
        /// <param name="page">Inicia no 0</param>
        /// <param name="pageCount">Quantidade padrão é 10</param>
        /// <returns></returns>
        [HttpGet("filter")]
        public async Task<IActionResult> FilterDiretoresAsync([FromQuery] string nome, [FromQuery] int page = 0, [FromQuery] int pageCount = 10)
        {
            try
            {
                return Ok(await DiretorService.FilterDiretoresAsync(nome, page, pageCount));
            }
            catch
            {
                return BadRequest("Erro ao listar os diretores");
            }
        }

        /// <summary>
        /// Retorna os detalhes do diretor
        /// </summary>
        /// <param name="idDiretor"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetDiretorById([FromQuery] string idDiretor)
        {
            try
            {
                return Ok(await DiretorService.GetById(idDiretor));
            }
            catch
            {
                return BadRequest("Erro ao buscar o diretor");
            }
        }

        /// <summary>
        /// Criar novo diretor
        /// </summary>
        /// <param name="diretor"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        public async Task<IActionResult> CreateNewDiretorAsync([FromBody] Diretor diretor)
        {
            try
            {
                if (!LoggedUser.Administrador)
                    return Unauthorized("Usuário não possui permissão para essa ação");

                return Ok(await DiretorService.InsertDiretorAsync(diretor));
            }
            catch (Exception)
            {
                return BadRequest("Erro ao salvar diretor");
            }
        }

        /// <summary>
        /// Atualizar diretor
        /// </summary>
        /// <param name="diretor"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPut]
        public async Task<IActionResult> UpdateDiretorAsync([FromBody] Diretor diretor)
        {
            try
            {
                if (!LoggedUser.Administrador)
                    return Unauthorized("Usuário não possui permissão para essa ação");

                return Ok(await DiretorService.UpdateDiretorAsync(diretor));
            }
            catch (Exception)
            {
                return BadRequest("Erro ao editar diretor");
            }
        }
    }
}
