﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerUI;
using System.Collections.Generic;
using System.IO;

namespace TesteTecnico.Ioasys.WebApi.Infrastructure.Configuration
{
    public static class SwaggerConfig
    {
        private const string AUTHORIZATION = "Authorization";
        private const string OAUTH2 = "oauth2";
        private const string TOKEN_TYPE = "Bearer";
        private const string TOKEN_DESCRIPTION = "JWT Authorization header using the Bearer scheme. \r\n\r\n Enter 'Bearer' [space] and then your token in the text input below.\r\n\r\nExample: \"Bearer 12345abcdef\"";

        public static void AddSwagger(this IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Teste Técnico Ioasys API", Version = "v1" });
                c.AddSecurityDefinition(TOKEN_TYPE, new OpenApiSecurityScheme
                {
                    Description = TOKEN_DESCRIPTION,
                    Name = AUTHORIZATION,
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = TOKEN_TYPE
                });
                c.AddSecurityRequirement(new OpenApiSecurityRequirement()
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = TOKEN_TYPE
                            },
                            Scheme = OAUTH2,
                            Name = TOKEN_TYPE,
                            In = ParameterLocation.Header,

                        },
                        new List<string>()
                    }
                });

                var filePath = Path.Combine(System.AppContext.BaseDirectory, "TesteTecnico.Ioasys.WebApi.xml");
                c.IncludeXmlComments(filePath);
            });
        }
        public static void AddSwaggerAppConfig(this IApplicationBuilder app)
        {
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Teste Técnico Ioasys API");
                c.RoutePrefix = string.Empty;
                c.DocExpansion(DocExpansion.None);
            });
            app.UseSwagger();
        }
    }
}
