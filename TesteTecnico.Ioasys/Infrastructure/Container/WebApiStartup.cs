﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using TesteTecnico.Ioasys.Domain.Contract;
using TesteTecnico.Ioasys.Domain.Contract.Repository;
using TesteTecnico.Ioasys.Domain.Contract.Service;
using TesteTecnico.Ioasys.Repository;
using TesteTecnico.Ioasys.Repository.Repository;
using TesteTecnico.Ioasys.Service;
using TesteTecnico.Ioasys.WebApi.Infrastructure.User;

namespace TesteTecnico.Ioasys.WebApi.Infrastructure.Container
{
    public class WebApiStartup
    {
        public static void Register(IServiceCollection services, IConfiguration configuration)
        {
            services.AddMemoryCache();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddDbContext<IoasysDbContext>(ServiceLifetime.Transient);

            using (var ctx = new IoasysDbContext(configuration))
            {
                ctx.Database.Migrate();
            }
            services.AddSingleton<ILoggedUser, LoggedUser>();

            RegisterServices(services);
            RegisterRepositories(services);
        }

        private static void RegisterServices(IServiceCollection services)
        {
            services.AddScoped<IAtorService, AtorService>();
            services.AddScoped<IBearerService, BearerService>();
            services.AddScoped<IDiretorService, DiretorService>();
            services.AddScoped<IFilmeService, FilmeService>();
            services.AddScoped<IGeneroService, GeneroService>();
            services.AddScoped<IUsuarioService, UsuarioService>();
        }

        private static void RegisterRepositories(IServiceCollection services)
        {
            services.AddScoped<IAtorRepository, AtorRepository>();
            services.AddScoped<IDiretorRepository, DiretorRepository>();
            services.AddScoped<IFilmeRepository, FilmeRepository>();
            services.AddScoped<IGeneroRepository, GeneroRepository>();
            services.AddScoped<IUsuarioRepository, UsuarioRepository>();
        }
    }
}
