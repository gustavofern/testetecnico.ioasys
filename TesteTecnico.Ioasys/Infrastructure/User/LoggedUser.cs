﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using TesteTecnico.Ioasys.Domain.Contract;

namespace TesteTecnico.Ioasys.WebApi.Infrastructure.User
{
    public class LoggedUser: ILoggedUser
    {
        private readonly IHttpContextAccessor httpContextAccessor;
        private const string authorizationHeader = "Authorization";
        private const string tokenTypeHeader = "Bearer ";

        public LoggedUser(IHttpContextAccessor httpContextAccessor) => this.httpContextAccessor = httpContextAccessor;


        public string IdUsuario => GetPropertie("idTenant");

        public bool Administrador => GetPropertie("Administrador") == "True" ? true : false;

        private IEnumerable<Claim>? GetClaims()
        {
            try
            {
                var jwt = GetJwt();
                if (jwt == null)
                {
                    return new List<Claim>();
                }
                var jwtHandler = new JwtSecurityTokenHandler();
                return jwtHandler.ReadToken(jwt) is JwtSecurityToken token ? token.Claims.ToList() : null;
            }
            catch (Exception ex)
            {
                return new List<Claim>();
            }
        }

        private string? GetJwt()
        {
            var authorization = httpContextAccessor.HttpContext.Request.Headers
                                                    .Where(h => h.Key.Equals(authorizationHeader, StringComparison.OrdinalIgnoreCase))
                                                    .SelectMany(h => h.Value);
            var beareToken = authorization.FirstOrDefault(b => b.StartsWith(tokenTypeHeader, StringComparison.OrdinalIgnoreCase));
            return !string.IsNullOrEmpty(beareToken) ? beareToken.Substring(7) : null;
        }

        private string GetPropertie(string? name = null)
        {
            var claims = GetClaims();
            return claims.FirstOrDefault(c => c.Type.Equals(name, StringComparison.OrdinalIgnoreCase))?.Value ?? string.Empty;
        }
    }
}
