﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TesteTecnico.Ioasys.Domain.Contract.Repository;
using TesteTecnico.Ioasys.Domain.Model;
using TesteTecnico.Ioasys.Domain.Model.Entity;

namespace TesteTecnico.Ioasys.Repository.Repository
{
    public class FilmeRepository : IFilmeRepository
    {
        private IoasysDbContext DbContext;

        public FilmeRepository(IoasysDbContext dbContext)
        {
            DbContext = dbContext;
        }

        public async Task<ListResponse<Filme>> FilterFilmesAsync(string nome, string diretor, List<string> generos, List<string> atores, int page = 0, int pageCount = 10)
        {
            var query = DbContext.Filme.AsQueryable();

            if (!string.IsNullOrEmpty(nome))
                query = query.Where(u => u.Nome.ToLower().Contains(nome.ToLower()));

            if (!string.IsNullOrEmpty(diretor))
                query = query.Where(u => u.IdDiretor == diretor);

            if (generos != null && generos.Count > 0)
            {
                query = query.Where(f => f.Generos.Any(g => generos.Contains(g.IdGenero)));
            }

            if (atores != null && atores.Count > 0)
            {
                query = query.Where(f => f.Atores.Any(g => atores.Contains(g.IdAtor)));
            }

            var response = new ListResponse<Filme>();

            response.Data = await query.OrderByDescending(f => f.QuantidadeVotos).ThenBy(f => f.Nome).Skip(page * pageCount).Take(pageCount).ToListAsync();
            response.Total = await query.CountAsync();

            return response;
        }

        public async Task<Filme> GetFilmeById(string idFilme)
        {
            return await DbContext.Filme.Include(f => f.Generos).ThenInclude(g => g.Genero).Include(f => f.Atores).ThenInclude(a => a.Ator).Where(u => u.IdFilme == idFilme).AsNoTracking().FirstOrDefaultAsync();
        }

        public async Task<Filme> InsertAsync(Filme filme)
        {
            DbContext.Filme.Add(filme);

            await DbContext.SaveChangesAsync();

            return filme;
        }

        public async Task<Filme> UpdateAsync(Filme filme)
        {
            DbContext.Filme.Update(filme);

            await DbContext.SaveChangesAsync();

            return filme;
        }
    }
}
