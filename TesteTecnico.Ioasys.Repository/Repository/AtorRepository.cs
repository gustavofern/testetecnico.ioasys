﻿using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using TesteTecnico.Ioasys.Domain.Contract.Repository;
using TesteTecnico.Ioasys.Domain.Model;
using TesteTecnico.Ioasys.Domain.Model.Entity;

namespace TesteTecnico.Ioasys.Repository.Repository
{
    public class AtorRepository : IAtorRepository
    {
        private IoasysDbContext DbContext;

        public AtorRepository(IoasysDbContext dbContext)
        {
            DbContext = dbContext;
        }

        public async Task<ListResponse<Ator>> FilterAtoresAsync(string nome, int page = 0, int pageCount = 10)
        {
            var response = new ListResponse<Ator>();
            var query = DbContext.Ator.AsQueryable();

            if (!string.IsNullOrEmpty(nome))
                query = query.Where(u => u.Nome.ToLower().Contains(nome.ToLower()));

            response.Data = await query.Skip(page * pageCount).Take(pageCount).ToListAsync();
            response.Total = await query.CountAsync();

            return response;
        }

        public async Task<Ator> GetAtorById(string idAtor)
        {
            return await DbContext.Ator.Where(u => u.IdAtor == idAtor).AsNoTracking().FirstOrDefaultAsync();
        }

        public async Task<Ator> InsertAsync(Ator ator)
        {
            DbContext.Ator.Add(ator);

            await DbContext.SaveChangesAsync();

            return ator;
        }

        public async Task<Ator> UpdateAsync(Ator ator)
        {
            DbContext.Ator.Update(ator);

            await DbContext.SaveChangesAsync();

            return ator;
        }
    }
}
