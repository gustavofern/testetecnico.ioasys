﻿using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using TesteTecnico.Ioasys.Domain.Contract.Repository;
using TesteTecnico.Ioasys.Domain.Model;
using TesteTecnico.Ioasys.Domain.Model.Entity;

namespace TesteTecnico.Ioasys.Repository.Repository
{
    public class GeneroRepository : IGeneroRepository
    {
        private IoasysDbContext DbContext;

        public GeneroRepository(IoasysDbContext dbContext)
        {
            DbContext = dbContext;
        }

        public async Task<ListResponse<Genero>> FilterGenerosAsync(string nome, int page = 0, int pageCount = 10)
        {
            var response = new ListResponse<Genero>();
            var query = DbContext.Genero.AsQueryable();

            if (!string.IsNullOrEmpty(nome))
                query = query.Where(u => u.Nome.ToLower().Contains(nome.ToLower()));

            response.Data = await query.Skip(page * pageCount).Take(pageCount).ToListAsync();
            response.Total = await query.CountAsync();

            return response;
        }

        public async Task<Genero> GetGeneroById(string idGenero)
        {
            return await DbContext.Genero.Where(u => u.IdGenero == idGenero).AsNoTracking().FirstOrDefaultAsync();
        }

        public async Task<Genero> InsertAsync(Genero genero)
        {
            DbContext.Genero.Add(genero);

            await DbContext.SaveChangesAsync();

            return genero;
        }

        public async Task<Genero> UpdateAsync(Genero genero)
        {
            DbContext.Genero.Update(genero);

            await DbContext.SaveChangesAsync();

            return genero;
        }
    }
}
