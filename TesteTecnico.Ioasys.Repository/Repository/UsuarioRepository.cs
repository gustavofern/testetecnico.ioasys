﻿using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using TesteTecnico.Ioasys.Domain.Contract.Repository;
using TesteTecnico.Ioasys.Domain.Model;
using TesteTecnico.Ioasys.Domain.Model.Entity;

namespace TesteTecnico.Ioasys.Repository.Repository
{
    public class UsuarioRepository : IUsuarioRepository
    {
        private IoasysDbContext DbContext;

        public UsuarioRepository(IoasysDbContext dbContext)
        {
            DbContext = dbContext;
        }

        public async Task<ListResponse<Usuario>> ListNonAdminUsersAsync(int page = 0, int pageCount = 10)
        {
            var response = new ListResponse<Usuario>();

            response.Data = await DbContext.Usuario.Where(u => u.Ativo && !u.Administrador).OrderBy(u => u.Nome).Skip(page * pageCount).Take(pageCount).ToListAsync();
            response.Total = await DbContext.Usuario.Where(u => u.Ativo && !u.Administrador).CountAsync();

            return response;
        }

        public async Task<Usuario> GetUsuarioByEmailSenha(string email, string senha)
        {
            return await DbContext.Usuario.Where(u => u.Email == email && u.Senha == senha && u.Ativo).FirstOrDefaultAsync();
        }

        public async Task<Usuario> GetUsuarioById(string idUsuario)
        {
            return await DbContext.Usuario.Where(u => u.IdUsuario == idUsuario && u.Ativo).AsNoTracking().FirstOrDefaultAsync();
        }

        public async Task<Usuario> InsertAsync(Usuario usuario)
        {
            DbContext.Usuario.Add(usuario);

            await DbContext.SaveChangesAsync();

            return usuario;
        }

        public async Task<Usuario> UpdateAsync(Usuario usuario)
        {
            DbContext.Usuario.Update(usuario);

            await DbContext.SaveChangesAsync();

            return usuario;
        }
    }
}
