﻿using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using TesteTecnico.Ioasys.Domain.Contract.Repository;
using TesteTecnico.Ioasys.Domain.Model;
using TesteTecnico.Ioasys.Domain.Model.Entity;

namespace TesteTecnico.Ioasys.Repository.Repository
{
    public class DiretorRepository: IDiretorRepository
    {
        private IoasysDbContext DbContext;

        public DiretorRepository(IoasysDbContext dbContext)
        {
            DbContext = dbContext;
        }

        public async Task<ListResponse<Diretor>> FilterDiretoresAsync(string nome, int page = 0, int pageCount = 10)
        {
            var response = new ListResponse<Diretor>();
            var query = DbContext.Diretor.AsQueryable();

            if (!string.IsNullOrEmpty(nome))
                query = query.Where(u => u.Nome.ToLower().Contains(nome.ToLower()));

            response.Data = await query.Skip(page * pageCount).Take(pageCount).ToListAsync();
            response.Total = await query.CountAsync();

            return response;
        }

        public async Task<Diretor> GetDiretorById(string idDiretor)
        {
            return await DbContext.Diretor.Where(u => u.IdDiretor == idDiretor).AsNoTracking().FirstOrDefaultAsync();
        }

        public async Task<Diretor> InsertAsync(Diretor diretor)
        {
            DbContext.Diretor.Add(diretor);

            await DbContext.SaveChangesAsync();

            return diretor;
        }

        public async Task<Diretor> UpdateAsync(Diretor diretor)
        {
            DbContext.Diretor.Update(diretor);

            await DbContext.SaveChangesAsync();

            return diretor;
        }
    }
}
