﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using TesteTecnico.Ioasys.Domain.Model.Entity;

namespace TesteTecnico.Ioasys.Repository
{
    public class IoasysDbContext : DbContext
    {

        private const string connectionString = "ConnectionStrings:Ioasys";
        private readonly IConfiguration configuration;

        public IoasysDbContext(IConfiguration configuration) => this.configuration = configuration;

        public DbSet<Ator> Ator { get; set; }
        public DbSet<AtorFilme> AtorFilme { get; set; }
        public DbSet<Diretor> Diretor { get; set; }
        public DbSet<Filme> Filme { get; set; }
        public DbSet<Genero> Genero { get; set; }
        public DbSet<GeneroFilme> GeneroFilme { get; set; }
        public DbSet<Usuario> Usuario { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder dbContextOptionsBuilder)
        {
            if (!dbContextOptionsBuilder.IsConfigured)
            {
                dbContextOptionsBuilder.UseMySQL(configuration[connectionString]);
            }
        }
    }
}
