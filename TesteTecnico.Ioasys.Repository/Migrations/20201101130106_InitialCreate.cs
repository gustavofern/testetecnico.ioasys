﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TesteTecnico.Ioasys.Repository.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ator",
                columns: table => new
                {
                    IdAtor = table.Column<string>(nullable: false),
                    Nome = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ator", x => x.IdAtor);
                });

            migrationBuilder.CreateTable(
                name: "diretor",
                columns: table => new
                {
                    IdDiretor = table.Column<string>(nullable: false),
                    Nome = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_diretor", x => x.IdDiretor);
                });

            migrationBuilder.CreateTable(
                name: "genero",
                columns: table => new
                {
                    IdGenero = table.Column<string>(nullable: false),
                    Nome = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_genero", x => x.IdGenero);
                });

            migrationBuilder.CreateTable(
                name: "usuario",
                columns: table => new
                {
                    IdUsuario = table.Column<string>(nullable: false),
                    Nome = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Senha = table.Column<string>(nullable: true),
                    Administrador = table.Column<bool>(nullable: false),
                    Ativo = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_usuario", x => x.IdUsuario);
                });

            migrationBuilder.CreateTable(
                name: "filme",
                columns: table => new
                {
                    IdFilme = table.Column<string>(nullable: false),
                    IdDiretor = table.Column<string>(nullable: true),
                    Nome = table.Column<string>(nullable: true),
                    Resumo = table.Column<string>(nullable: true),
                    DataLancamento = table.Column<DateTime>(nullable: false),
                    QuantidadeVotos = table.Column<int>(nullable: false),
                    QualificacaoTotal = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_filme", x => x.IdFilme);
                    table.ForeignKey(
                        name: "FK_filme_diretor_IdDiretor",
                        column: x => x.IdDiretor,
                        principalTable: "diretor",
                        principalColumn: "IdDiretor",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "atorfilme",
                columns: table => new
                {
                    IdAtorFilme = table.Column<string>(nullable: false),
                    IdAtor = table.Column<string>(nullable: true),
                    IdFilme = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_atorfilme", x => x.IdAtorFilme);
                    table.ForeignKey(
                        name: "FK_atorfilme_ator_IdAtor",
                        column: x => x.IdAtor,
                        principalTable: "ator",
                        principalColumn: "IdAtor",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_atorfilme_filme_IdFilme",
                        column: x => x.IdFilme,
                        principalTable: "filme",
                        principalColumn: "IdFilme",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "generofilme",
                columns: table => new
                {
                    IdGeneroFilme = table.Column<string>(nullable: false),
                    IdGenero = table.Column<string>(nullable: true),
                    IdFilme = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_generofilme", x => x.IdGeneroFilme);
                    table.ForeignKey(
                        name: "FK_generofilme_filme_IdFilme",
                        column: x => x.IdFilme,
                        principalTable: "filme",
                        principalColumn: "IdFilme",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_generofilme_genero_IdGenero",
                        column: x => x.IdGenero,
                        principalTable: "genero",
                        principalColumn: "IdGenero",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_atorfilme_IdAtor",
                table: "atorfilme",
                column: "IdAtor");

            migrationBuilder.CreateIndex(
                name: "IX_atorfilme_IdFilme",
                table: "atorfilme",
                column: "IdFilme");

            migrationBuilder.CreateIndex(
                name: "IX_filme_IdDiretor",
                table: "filme",
                column: "IdDiretor");

            migrationBuilder.CreateIndex(
                name: "IX_generofilme_IdFilme",
                table: "generofilme",
                column: "IdFilme");

            migrationBuilder.CreateIndex(
                name: "IX_generofilme_IdGenero",
                table: "generofilme",
                column: "IdGenero");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "atorfilme");

            migrationBuilder.DropTable(
                name: "generofilme");

            migrationBuilder.DropTable(
                name: "usuario");

            migrationBuilder.DropTable(
                name: "ator");

            migrationBuilder.DropTable(
                name: "filme");

            migrationBuilder.DropTable(
                name: "genero");

            migrationBuilder.DropTable(
                name: "diretor");
        }
    }
}
