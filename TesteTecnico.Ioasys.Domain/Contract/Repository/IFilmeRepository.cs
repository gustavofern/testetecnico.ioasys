﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TesteTecnico.Ioasys.Domain.Model;
using TesteTecnico.Ioasys.Domain.Model.Entity;

namespace TesteTecnico.Ioasys.Domain.Contract.Repository
{
    public interface IFilmeRepository
    {
        Task<ListResponse<Filme>> FilterFilmesAsync(string nome, string diretor, List<string> generos, List<string> atores, int page = 0, int pageCount = 10);
        Task<Filme> GetFilmeById(string idFilme);
        Task<Filme> InsertAsync(Filme filme);
        Task<Filme> UpdateAsync(Filme filme);
    }
}
