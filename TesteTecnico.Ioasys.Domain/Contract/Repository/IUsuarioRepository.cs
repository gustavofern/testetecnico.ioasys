﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TesteTecnico.Ioasys.Domain.Model;
using TesteTecnico.Ioasys.Domain.Model.Entity;

namespace TesteTecnico.Ioasys.Domain.Contract.Repository
{
    public interface IUsuarioRepository
    {
        Task<ListResponse<Usuario>> ListNonAdminUsersAsync(int page = 0, int pageCount = 10);
        Task<Usuario> GetUsuarioByEmailSenha(string email, string senha);
        Task<Usuario> GetUsuarioById(string idUsuario);
        Task<Usuario> InsertAsync(Usuario usuario);
        Task<Usuario> UpdateAsync(Usuario usuario);
    }
}
