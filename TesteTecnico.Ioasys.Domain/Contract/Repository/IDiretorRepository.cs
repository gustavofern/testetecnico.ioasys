﻿using System.Threading.Tasks;
using TesteTecnico.Ioasys.Domain.Model;
using TesteTecnico.Ioasys.Domain.Model.Entity;

namespace TesteTecnico.Ioasys.Domain.Contract.Repository
{
    public interface IDiretorRepository
    {
        Task<ListResponse<Diretor>> FilterDiretoresAsync(string nome, int page = 0, int pageCount = 10);
        Task<Diretor> GetDiretorById(string idDiretor);
        Task<Diretor> InsertAsync(Diretor diretor);
        Task<Diretor> UpdateAsync(Diretor diretor);
    }
}
