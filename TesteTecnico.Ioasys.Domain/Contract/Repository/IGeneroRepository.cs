﻿using System.Threading.Tasks;
using TesteTecnico.Ioasys.Domain.Model;
using TesteTecnico.Ioasys.Domain.Model.Entity;

namespace TesteTecnico.Ioasys.Domain.Contract.Repository
{
    public interface IGeneroRepository
    {
        Task<ListResponse<Genero>> FilterGenerosAsync(string nome, int page = 0, int pageCount = 10);
        Task<Genero> GetGeneroById(string idGenero);
        Task<Genero> InsertAsync(Genero genero);
        Task<Genero> UpdateAsync(Genero genero);
    }
}
