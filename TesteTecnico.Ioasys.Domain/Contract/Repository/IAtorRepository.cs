﻿using System.Threading.Tasks;
using TesteTecnico.Ioasys.Domain.Model;
using TesteTecnico.Ioasys.Domain.Model.Entity;

namespace TesteTecnico.Ioasys.Domain.Contract.Repository
{
    public interface IAtorRepository
    {
        Task<ListResponse<Ator>> FilterAtoresAsync(string nome, int page = 0, int pageCount = 10);
        Task<Ator> GetAtorById(string idAtor);
        Task<Ator> InsertAsync(Ator ator);
        Task<Ator> UpdateAsync(Ator ator);
    }
}
