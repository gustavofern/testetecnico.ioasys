﻿namespace TesteTecnico.Ioasys.Domain.Contract
{
    public interface ILoggedUser
    {
        string IdUsuario { get; }

        bool Administrador { get; }
    }
}
