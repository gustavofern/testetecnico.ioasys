﻿using System.Threading.Tasks;
using TesteTecnico.Ioasys.Domain.Model;
using TesteTecnico.Ioasys.Domain.Model.Entity;

namespace TesteTecnico.Ioasys.Domain.Contract.Service
{
    public interface IAtorService
    {
        Task<ListResponse<Ator>> FilterAtoresAsync(string nome, int page = 0, int pageCount = 10);
        Task<Ator> GetById(string idAtor);
        Task<Ator> InsertAtorAsync(Ator ator);
        Task<Ator> UpdateAtorAsync(Ator ator);
    }
}
