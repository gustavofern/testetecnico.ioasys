﻿using System.Threading.Tasks;
using TesteTecnico.Ioasys.Domain.Model;
using TesteTecnico.Ioasys.Domain.Model.Entity;

namespace TesteTecnico.Ioasys.Domain.Contract.Service
{
    public interface IUsuarioService
    {
        Task<Usuario> GetByEmailSenha(string email, string senha);
        Task<Usuario> GetById(string idUsuario);
        Task<ListResponse<Usuario>> ListNonAdminUsersAsync(int page = 0, int pageCount = 10);
        Task<Usuario> InsertUsuarioAsync(Usuario usuario);
        Task<Usuario> UpdateUsuarioAsync(Usuario usuario);
        Task<Usuario> DisableUsuarioAsync(string idUsuario);
    }
}
