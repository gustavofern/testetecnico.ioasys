﻿using System.Threading.Tasks;
using TesteTecnico.Ioasys.Domain.Model;
using TesteTecnico.Ioasys.Domain.Model.Entity;

namespace TesteTecnico.Ioasys.Domain.Contract.Service
{
    public interface IDiretorService
    {
        Task<ListResponse<Diretor>> FilterDiretoresAsync(string nome, int page = 0, int pageCount = 10);
        Task<Diretor> GetById(string idDiretor);
        Task<Diretor> InsertDiretorAsync(Diretor diretor);
        Task<Diretor> UpdateDiretorAsync(Diretor diretor);
    }
}
