﻿using System.Threading.Tasks;
using TesteTecnico.Ioasys.Domain.Model;
using TesteTecnico.Ioasys.Domain.Model.Entity;

namespace TesteTecnico.Ioasys.Domain.Contract.Service
{
    public interface IGeneroService
    {
        Task<ListResponse<Genero>> FilterGenerosAsync(string nome, int page = 0, int pageCount = 10);
        Task<Genero> GetById(string idGenero);
        Task<Genero> InsertGeneroAsync(Genero genero);
        Task<Genero> UpdateGeneroAsync(Genero genero);
    }
}
