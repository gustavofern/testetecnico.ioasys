﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TesteTecnico.Ioasys.Domain.Model;
using TesteTecnico.Ioasys.Domain.Model.Entity;
using TesteTecnico.Ioasys.Domain.Model.RO;

namespace TesteTecnico.Ioasys.Domain.Contract.Service
{
    public interface IFilmeService
    {
        Task<Filme> GetById(string idFilme);
        Task<ListResponse<Filme>> FilterFilmesAsync(string nome, string diretor, List<string> generos, List<string> atores, int page = 0, int pageCount = 10);
        Task<Filme> VoteAsync(string idFilme, int nota);
        Task<Filme> InsertFilmeAsync(NewFilmeRequestObject requestObj);
        Task<Filme> UpdateFilmeAsync(Filme filme);
    }
}
