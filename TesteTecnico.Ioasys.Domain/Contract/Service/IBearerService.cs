﻿using TesteTecnico.Ioasys.Domain.Model.Entity;

namespace TesteTecnico.Ioasys.Domain.Contract.Service
{
    public interface IBearerService
    {
        dynamic GenerateBearer(Usuario usuario);
    }
}
