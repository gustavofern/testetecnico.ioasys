﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TesteTecnico.Ioasys.Domain.Model
{
    public class ListResponse<T>
    {
        public List<T> Data { get; set; }
        public int Total { get; set; }
    }
}
