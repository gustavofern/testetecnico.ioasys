﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TesteTecnico.Ioasys.Domain.Model.Entity
{
    [Table("genero")]
    public class Genero
    {
        [Key]
        public string IdGenero { get; set; }
        public string Nome { get; set; }

        public Genero()
        {
            IdGenero = Guid.NewGuid().ToString();
        }
    }
}
