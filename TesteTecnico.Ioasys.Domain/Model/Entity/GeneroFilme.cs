﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TesteTecnico.Ioasys.Domain.Model.Entity
{
    [Table("generofilme")]
    public class GeneroFilme
    {
        [Key]
        public string IdGeneroFilme { get; set; }
        [ForeignKey(nameof(Genero))]
        public string IdGenero { get; set; }
        [ForeignKey(nameof(Filme))]
        public string IdFilme { get; set; }

        public Genero Genero { get; set; }
        public Filme Filme { get; set; }

        public GeneroFilme()
        {
            IdGeneroFilme = Guid.NewGuid().ToString();
        }
    }
}
