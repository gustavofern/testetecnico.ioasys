﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TesteTecnico.Ioasys.Domain.Model.Entity
{
    [Table("atorfilme")]
    public class AtorFilme
    {
        [Key]
        public string IdAtorFilme { get; set; }
        [ForeignKey(nameof(Ator))]
        public string IdAtor { get; set; }
        [ForeignKey(nameof(Filme))]
        public string IdFilme { get; set; }

        public Ator Ator { get; set; }
        public Filme Filme { get; set; }

        public AtorFilme()
        {
            IdAtorFilme = Guid.NewGuid().ToString();
        }
    }
}
