﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TesteTecnico.Ioasys.Domain.Model.Entity
{
    [Table("ator")]
    public class Ator
    {
        [Key]
        public string IdAtor { get; set; }
        public string Nome { get; set; }

        public Ator()
        {
            IdAtor = Guid.NewGuid().ToString();
        }
    }
}