﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TesteTecnico.Ioasys.Domain.Model.Entity
{
    [Table("filme")]
    public class Filme
    {
        [Key]
        public string IdFilme { get; set; }
        [ForeignKey(nameof(Diretor))]
        public string IdDiretor { get; set; }
        public string Nome { get; set; }
        public string Resumo { get; set; }
        public DateTime DataLancamento { get; set; }

        public Diretor Diretor { get; set; }
        public List<AtorFilme> Atores { get; set; }
        public List<GeneroFilme> Generos { get; set; }

        public int QuantidadeVotos { get; set; }
        public int QualificacaoTotal { get; set; }
        [NotMapped]
        public double MediaVotos { get { return QuantidadeVotos > 0 ? QualificacaoTotal / (double)QuantidadeVotos : 0.0; } }

        public Filme()
        {
            IdFilme = Guid.NewGuid().ToString();

            Atores = new List<AtorFilme>();
            Generos = new List<GeneroFilme>();
        }
    }
}
