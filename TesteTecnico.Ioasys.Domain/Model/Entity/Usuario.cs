﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TesteTecnico.Ioasys.Domain.Model.Entity
{
    [Table("usuario")]
    public class Usuario
    {
        [Key]
        public string IdUsuario { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Senha { get; set; }

        public bool Administrador { get; set; }
        public bool Ativo { get; set; }

        public Usuario()
        {
            IdUsuario = Guid.NewGuid().ToString();
        }
    }
}
