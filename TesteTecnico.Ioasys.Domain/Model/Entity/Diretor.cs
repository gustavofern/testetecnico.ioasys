﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TesteTecnico.Ioasys.Domain.Model.Entity
{
    [Table("diretor")]
    public class Diretor
    {
        [Key]
        public string IdDiretor { get; set; }
        public string Nome { get; set; }

        public Diretor()
        {
            IdDiretor = Guid.NewGuid().ToString();
        }
    }
}
