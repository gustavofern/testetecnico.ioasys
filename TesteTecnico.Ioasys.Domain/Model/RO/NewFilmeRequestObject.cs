﻿using System;
using System.Collections.Generic;

namespace TesteTecnico.Ioasys.Domain.Model.RO
{
    public class NewFilmeRequestObject
    {
        public string Nome { get; set; }
        public string Resumo { get; set; }
        public string idDiretor { get; set; }
        public DateTime DataLancamento { get; set; }

        /// <summary>
        /// Lista com Id dos atores
        /// </summary>
        public List<string> Atores { get; set; }

        /// <summary>
        /// Lista com Id dos generos
        /// </summary>
        public List<string> Generos { get; set; }
    }
}
